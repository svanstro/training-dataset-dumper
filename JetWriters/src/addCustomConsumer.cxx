#include "JetWriters/addCustomConsumer.h"
#include "FlavorTagDiscriminants/CustomGetterUtils.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODCaloEvent/CaloCluster.h"


// because customSequenceGetterWithDeps is defined in the CustomGetterUtils.cxx source file
// we need to declare explcitly declare the template instantiation here. In future we should
// define the function alongside the definition in CustomGetterUtils.h
namespace FlavorTagDiscriminants {
  namespace getter_utils {
    extern template
    std::pair<std::function<std::vector<double>(const xAOD::Jet&, const std::vector<const xAOD::TrackParticle*>&)>, 
              std::set<std::string>>
    buildCustomSeqGetter<xAOD::TrackParticle>(const std::string&, const std::string&);
  }
}



namespace {

  using namespace jetwriters::detail;


  // wrapper class to convert the things we have in
  // FlavorTagDiscriminants into something that HDF5Utils can read.
  template <typename T>
  class CustomSeqWrapper
  {

  public:
    CustomSeqWrapper(const std::string& name, const std::string& prefix) {
      namespace gu = FlavorTagDiscriminants::getter_utils;
      using TP = xAOD::TrackParticle;
      auto getterPair = gu::buildCustomSeqGetter<TP>(name, prefix);
      m_getter = getterPair.first;
    }

    T operator()(const JetPair<xAOD::TrackParticle>& t) {
      auto output_vec = m_getter(*t.second, {t.first});
      return output_vec.at(0);
    }

  private:
    std::function<std::vector<double>(const xAOD::Jet&, const std::vector<const xAOD::TrackParticle*>&)> m_getter;
  };

  const std::string g_ip_prefix = "ip_prefix";

} // namespace


namespace jetwriters::detail {

  template <>
  JetGetter<xAOD::TrackParticle, float> getFloat<xAOD::TrackParticle>(
    const std::string& name,
    EdmNameMap& edmName)
  {

    std::string prefix = edmName(g_ip_prefix);
    if (prefix == g_ip_prefix) {
      throw std::runtime_error(
        "must specify 'ip_prefix' to use '" + name + "'");
    }

    if (name == "d0RelativeToBeamspot"){
      return [](const JetPair<xAOD::TrackParticle>& t) -> float {
        return t.first->d0();
      };
    } else if (name == "d0RelativeToBeamspotUncertainty") {
      return [](const JetPair<xAOD::TrackParticle>& t) -> float {
        return std::sqrt(
          t.first->definingParametersCovMatrixDiagVec().at(0));
      };
    } else if (name == "d0RelativeToBeamspotSignificance"){
      return [](const JetPair<xAOD::TrackParticle>& t) -> float {
        return t.first->d0() / std::sqrt(
          t.first->definingParametersCovMatrixDiagVec().at(0));
      };
      // There are a few cases here where we'd like to migrate to a
      // new name. I'm doing it within the dumpser but we should
      // remove these special cases as soon as the MR to add these to
      // Athena is in our release. See the MR here:
      //
      // https://gitlab.cern.ch/atlas/athena/-/merge_requests/76048
      //
    } else if (name == "lifetimeSignedD0") {
      return CustomSeqWrapper<float>("IP3D_signed_d0", prefix);
    } else if (name == "lifetimeSignedZ0SinTheta") {
      return CustomSeqWrapper<float>("IP3D_signed_z0", prefix);
    } else if (name == "lifetimeSignedD0Significance") {
      return CustomSeqWrapper<float>("IP3D_signed_d0_significance", prefix);
    } else if (name == "lifetimeSignedZ0SinThetaSignificance") {
      return CustomSeqWrapper<float>("IP3D_signed_z0_significance", prefix);
    } else {
      // most of the custom getters can be grabbed from Athena
      // note that within FlavorTagDiscriminants all the floats are
      // double we're truncating here because that precision is
      // probably not needed
      return CustomSeqWrapper<float>(name, prefix);
    }
  }

  template <>
  JetGetter<xAOD::CaloCluster, float> getFloat<xAOD::CaloCluster>(
    const std::string& name,
    EdmNameMap&)
  {

    if (name == "etaSize") {
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        return t.first->getClusterEtaSize();
      };
    } else if (name == "phiSize"){
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        return t.first->getClusterPhiSize();
      };
    } else if (name == "ncells"){
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        return t.first->numberCells();
      };
    } else if (name == "FRAC_HAD"){
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        double value;
        t.first->retrieveMoment(xAOD::CaloCluster_v1::MomentType::ENG_CALIB_FRAC_HAD, value);
        return value;
      };
    } else {
      return std::nullopt;;
    }
  }

  template <>
  JetGetter<xAOD::FlowElement, bool> getBool<xAOD::FlowElement>(
    const std::string& name,
    EdmNameMap&)
  {

    if (name == "isCharged") {
      return [](const JetPair<xAOD::FlowElement>& t) -> bool {
        return t.first->isCharged();
      };
    } else {
      return std::nullopt;;
    }
  }

  EdmNameMap::EdmNameMap(const std::map<std::string, std::string>& in):
    m_remap(in)
  {
  }
  std::string EdmNameMap::operator()(const std::string& in)
  {
    if (m_remap.count(in)) {
      m_used.insert(in);
      return m_remap.at(in);
    }
    return in;
  }
  // checks to ensure that all varialbe name replacements are used
  void EdmNameMap::checkRemaining()
  {
    auto edm_name = m_remap;
    for (const auto& k: m_used) edm_name.erase(k);
    // ip prefix is kind of a special case: it might be a required
    // parameter even if it's not used to make a variable
    edm_name.erase(g_ip_prefix);
    if (!edm_name.empty()) {
      std::string error = "Found unused name substitutions: ";
      std::string unused;
      for (auto& name_pair: edm_name) {
        if (!unused.empty()) unused.append(", ");
        unused.append(
          "'" + name_pair.first + "'->'" + name_pair.second + "'");
      }
      throw std::runtime_error(error + unused);
    }
  }
} // namespace jetwriters::detail
