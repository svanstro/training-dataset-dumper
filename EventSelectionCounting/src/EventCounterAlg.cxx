#include "EventSelectionCounting/EventCounterAlg.h"

#include <nlohmann/json.hpp>

#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;

EventCounterAlg::EventCounterAlg(const std::string &name, ISvcLocator* loc)
  : AthAlgorithm(name, loc), m_count(0)
{}

StatusCode EventCounterAlg::initialize() {
  if (m_count_name.value().empty()) {
    ATH_MSG_ERROR("Event count ID not given");
    return StatusCode::FAILURE;
  }

  if (m_counts_output_path.value().empty()) {
    ATH_MSG_ERROR("Event counter file name not given");
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode EventCounterAlg::execute() {
  ++m_count;
  return StatusCode::SUCCESS;
}

StatusCode EventCounterAlg::finalize() {
  using CountsMap = std::vector<std::pair<std::string, unsigned long long>>;

  nlohmann::json counts_json;
  if (fs::exists(m_counts_output_path.value())) {
    std::ifstream counts_stream(m_counts_output_path.value());
    counts_json = nlohmann::json::parse(counts_stream);
  } else {
    counts_json = CountsMap();
  }

  for (const auto& [name, count] : counts_json.get<CountsMap>()) {
    if (name == m_count_name.value()) {
      ATH_MSG_ERROR("count name " << m_count_name.value()
          << " already exists in " << m_counts_output_path.value());
      return StatusCode::FAILURE;
    }
  }

  counts_json.push_back({m_count_name.value(), m_count});

  std::ofstream counts_stream(m_counts_output_path.value());
  counts_stream << counts_json.dump(4);
  return StatusCode::SUCCESS;
}
