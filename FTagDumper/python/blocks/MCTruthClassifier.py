from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class MCTruthClassifier(BaseBlock):

    name_mctc: str = 'DumperMCTruthClassifier'
    truth_particle_container: str = 'TruthParticles'
    dec_name: str = "MCTCDecoratorAlg"

    def to_ca(self):
        ca = ComponentAccumulator()
        truthClassifier = CompFactory.MCTruthClassifier(
            name=self.name_mctc,
            xAODTruthParticleContainerName=self.truth_particle_container,
        )

        truthClassifierAlg = CompFactory.MCTCDecoratorAlg(
            name=self.dec_name,
            MCTruthClassifier=truthClassifier,
            truthContainer=self.truth_particle_container,
        )
        ca.addEventAlgo(truthClassifierAlg)
        return ca