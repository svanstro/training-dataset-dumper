# Open dataset samples
# https://ftag.docs.cern.ch/software/samples/

# -------------------------------------------------------------------------------------------------
# New training samples: mc23a and mc23c (https://its.cern.ch/jira/browse/ATLFTAGDPD-395)
# -------------------------------------------------------------------------------------------------
mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8549_s4159_r14799_p6368
mc23_13p6TeV.601589.PhPy8EG_A14_ttbar_hdamp258p75_nonallhadron.deriv.DAOD_FTAG1.e8549_s4162_r14622_p6368

# ------------------------------------
# Default run 3 training samples: mc23a
# ------------------------------------
# ttbar
#mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_FTAG1.e8514_s4162_r15687_p6368
#mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_FTAG1.e8514_s4159_r15530_p6368

