#ifndef HIT_WRITER_CONFIG_HH
#define HIT_WRITER_CONFIG_HH

#include <string>
#include <vector>

struct HitWriterConfig {
  std::string name;
  std::size_t output_size=0;
  float dist_to_jet;
  bool save_endcap_hits;
  bool save_only_clean_hits;
  bool do_overlap_removal;
  bool save_sct;
  std::vector<std::string> variables;
  enum class Save {UNKNOWN, All, InTrack, NotInTrack};
  Save save_hits;
  bool force_full_precision = false;
};


#endif
