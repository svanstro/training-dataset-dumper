#include "BJetShallowCopier.hh"
#include "SafeShallowCopy.hh"

#include "xAODJet/JetContainer.h"
#include "xAODBTagging/BTaggingContainer.h"

namespace {
  template <typename T>
  using Acc_t = SG::AuxElement::ConstAccessor<T>;
  template <typename T>
  using Dec_t = SG::AuxElement::Decorator<T>;
  using JetLink_t = ElementLink<xAOD::JetContainer>;
  using BTagLink_t = ElementLink<xAOD::BTaggingContainer>;

}

static const std::string JET_LINK_NAME = "jetLink";
struct ShallowCopyLinkers
{
  Acc_t<JetLink_t> jetAcc;
  Dec_t<JetLink_t> jetDec;
  Acc_t<BTagLink_t> btagAcc;
  Dec_t<BTagLink_t> btagDec;
  ShallowCopyLinkers(const std::string& btag_link):
    jetAcc(JET_LINK_NAME),
    jetDec(JET_LINK_NAME),
    btagAcc(btag_link.empty() ? "dummy_btag_link": btag_link),
    btagDec(btag_link.empty() ? "dummy_btag_link": btag_link)
    {}
};

BJetShallowCopier::BJetShallowCopier(const std::string& btag_link):
  m_links(new ShallowCopyLinkers(btag_link))
{
  if(btag_link.empty()) m_copyBtagObject = false;
}

BJetShallowCopier::~BJetShallowCopier() = default;

BJetShallowCopier::BJetCopy BJetShallowCopier::shallowCopyBJets(
  const xAOD::JetContainer& original_jets) const {

  auto [jets, jetAux] = safeShallowCopyContainer(original_jets);

  // check if we want only a copy of the jets
  if (!m_copyBtagObject) {
    BJetShallowAux aux;
    aux.jetAux = std::move(jetAux);
    return std::make_pair(std::move(jets), std::move(aux));
  }

  // assuming we have at least one b-jet with a valid link, we can get
  // the b-tagging container
  const xAOD::BTaggingContainer* original_btags = nullptr;
  for (const auto& jet: *jets) {
    const auto& btag_link = m_links->btagAcc(*jet);
    if (btag_link.isValid()) {
      const auto* btags = btag_link.getStorableObjectPointer();
      if (original_btags) {
        if (original_btags != btags) {
          throw std::logic_error("found inconsistent btagging links");
        }
      } else {
        original_btags = btags;
      }
    }
  }
  // if we weren't able to find a valid b-tagging link, just return
  // the jets (this might just mean the jet container was empty)
  if (!original_btags) {
    BJetShallowAux aux;
    aux.jetAux = std::move(jetAux);
    return std::make_pair(std::move(jets), std::move(aux));
  }
  // shallow copy the b-tagging container
  auto [btags, btagAux] = safeShallowCopyContainer(*original_btags);

  // Now we have to update the links. Each b-tagging object should
  // point to a jet, so we need to:
  //
  // - loop over the new b-tagging container
  //
  // - find the index that the `jetLink` points to
  //
  // - overwrite the `jetLink` to point to the new jet container, with the
  //   same index.
  //
  // - Retrieve the jet at this jet index, overwrite the
  //   `btaggingLink` to point to the new b-tagging container and the
  //   index of the current b-tagging object
  //
  // This has to be done in two steps because writing new element
  // links invalidates all the old links.
  std::map<std::size_t,std::size_t> jet_index_from_btag_index;
  for (const auto& btag: *btags) {
    JetLink_t old_jet_link = m_links->jetAcc(*btag);
    std::size_t jet_index = (*old_jet_link)->index();
    jet_index_from_btag_index[btag->index()] = jet_index;
  }

  for (const auto& btag: *btags) {
    std::size_t jet_index = jet_index_from_btag_index.at(btag->index());
    JetLink_t new_jet_link(*jets, jet_index);
    m_links->jetDec(*btag) = new_jet_link;
    const xAOD::Jet* new_jet = jets->at(jet_index);
    BTagLink_t new_btag_link(*btags, btag->index());
    m_links->btagDec(*new_jet) = new_btag_link;
  }
  BJetShallowAux aux;
  aux.jetAux = std::move(jetAux);
  aux.btag = std::move(btags);
  aux.btagAux = std::move(btagAux);
  return std::make_pair(std::move(jets), std::move(aux));
}
