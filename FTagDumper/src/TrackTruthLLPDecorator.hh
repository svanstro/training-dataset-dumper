#ifndef TRACK_TRUTH_LLP_DECORATOR_HH
#define TRACK_TRUTH_LLP_DECORATOR_HH

#include "TrackSelector.hh"

#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"

#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthEventContainer.h"

#include <string>
#include <math.h>

class TrackTruthLLPDecorator
{
public:
  TrackTruthLLPDecorator(const std::string& decorator_prefix = "",
                      const std::string& tool_prefix = "",
			 const std::vector<int>& pdgIdList = {});

  // this is the function that actually does the decoration
  void decorateAll(TrackSelector::Tracks tracks, 
                   const xAOD::TruthVertex* truth_PV) const;

  // helper functions
  static bool sort_tracks(const xAOD::TrackParticle* track_A , const xAOD::TrackParticle* track_B );
  int checkProduction( const xAOD::TruthParticle & truthPart, int counter ) const;
  const xAOD::TruthVertex* get_truth_vertex( const xAOD::TrackParticle* track ) const;
  const xAOD::TruthVertex* get_nearest_vertex(const xAOD::TruthVertex* search_vertex, std::vector<const xAOD::TruthVertex*> vertices) const;
  
private:
  float min_dr_to_merge = 0.1; // mm

  SG::AuxElement::Decorator<int> m_track_origin_label;
  SG::AuxElement::Decorator<int> m_track_production_vertex_idx;
  SG::AuxElement::Decorator<int> m_track_truth_barcode;
  SG::AuxElement::Decorator<int> m_track_parent_barcode;

  InDet::InDetTrackTruthOriginTool m_InDetTrackTruthOriginTool;

  std::vector<int> m_pdgIdList;

  enum ExclusiveOriginLLP {
    Pileup,
    Fake,
    Primary,
    FromHF, // (FromB, FromBC, FromC)
    FromTau,
    FromLLP,
    OtherSecondary,
    Unknown
  };
};

#endif
