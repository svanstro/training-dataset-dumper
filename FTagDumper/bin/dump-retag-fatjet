#!/usr/bin/env python3

from argparse import ArgumentParser
import sys

from FTagDumper.dumper import getMainConfig as getConfig
from AthenaConfiguration.AllConfigFlags import initConfigFlags
from FTagDumper import dumper
from FTagDumper import retagfatjet

def get_args():
    """
    Extend the base dumper argument parser.
    """
    parser = ArgumentParser(
        formatter_class=dumper.DumperHelpFormatter,
        parents=[dumper.base_parser(__doc__, add_help=False)],
    )
    track_opts = parser.add_mutually_exclusive_group()

    track_opts.add_argument(
        "-M",
        "--merge-tracks",
        action="store_true",
        help="merge the standard and large d0 track collections before everything else.\n"
             "only works in releases after 22.0.59 (2022-03-08T2101)",
    )

    return parser.parse_args()

def run():

    args = get_args()

    track_collection="InDetTrackParticles",
    jet_collection="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"
    flags = initConfigFlags()
    flags = dumper.update_flags(args, flags)
    flags.lock()

    ca = getConfig(flags)

    config = dumper.combinedConfig(args.config_file)
    ca.merge(
        retagfatjet.AddTrackSys_largeR(
            flags,
            track_collection,
            config['track_systematics'],
            jet_collection,
        )
    )

    ca.merge(dumper.getDumperConfig(args, config_dict=config['dumper']))


    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
